/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.solemn3.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author csanjuana
 */
@Entity
@Table(name = "infovacuna")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Infovacuna.findAll", query = "SELECT i FROM Infovacuna i"),
    @NamedQuery(name = "Infovacuna.findByRut", query = "SELECT i FROM Infovacuna i WHERE i.rut = :rut"),
    @NamedQuery(name = "Infovacuna.findByNombre", query = "SELECT i FROM Infovacuna i WHERE i.nombre = :nombre"),
    @NamedQuery(name = "Infovacuna.findByComuna", query = "SELECT i FROM Infovacuna i WHERE i.comuna = :comuna"),
    @NamedQuery(name = "Infovacuna.findByTelefono", query = "SELECT i FROM Infovacuna i WHERE i.telefono = :telefono"),
    @NamedQuery(name = "Infovacuna.findByVacuna", query = "SELECT i FROM Infovacuna i WHERE i.vacuna = :vacuna")})
public class Infovacuna implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "rut")
    private String rut;
    @Size(max = 50)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 50)
    @Column(name = "comuna")
    private String comuna;
    @Size(max = 50)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 50)
    @Column(name = "vacuna")
    private String vacuna;

    public Infovacuna() {
    }

    public Infovacuna(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getVacuna() {
        return vacuna;
    }

    public void setVacuna(String vacuna) {
        this.vacuna = vacuna;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Infovacuna)) {
            return false;
        }
        Infovacuna other = (Infovacuna) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.solemn3.entity.Infovacuna[ rut=" + rut + " ]";
    }
    
}
