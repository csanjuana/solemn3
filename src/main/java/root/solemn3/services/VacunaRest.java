/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.solemn3.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.solemn3.dao.InfovacunaJpaController;
import root.solemn3.dao.exceptions.NonexistentEntityException;
import root.solemn3.entity.Infovacuna;

/**
 *
 * @author csanjuana
 */
@Path("vacunas")
public class VacunaRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarVacunas() {

        InfovacunaJpaController dao = new InfovacunaJpaController();

        List<Infovacuna> vacunas = dao.findInfovacunaEntities();
        return Response.ok(200).entity(vacunas).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Infovacuna vacunas) {

        InfovacunaJpaController dao = new InfovacunaJpaController();
        try {
            dao.create(vacunas);
        } catch (Exception ex) {
            Logger.getLogger(VacunaRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(vacunas).build();

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Infovacuna vacunas) {

        InfovacunaJpaController dao = new InfovacunaJpaController();
        try {
            dao.edit(vacunas);
        } catch (Exception ex) {
            Logger.getLogger(VacunaRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(vacunas).build();
    }

    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar(@PathParam("ideliminar") String ideliminar) {

        InfovacunaJpaController dao = new InfovacunaJpaController();
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(VacunaRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity("Persona eliminada").build();
    }

    @GET
    @Path("/{idConsultar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarporId(@PathParam("idConsultar") String idConsultar) {

        InfovacunaJpaController dao = new InfovacunaJpaController();
        Infovacuna vacunas = dao.findInfovacuna(idConsultar);

        return Response.ok(200).entity(vacunas).build();
    }

}
