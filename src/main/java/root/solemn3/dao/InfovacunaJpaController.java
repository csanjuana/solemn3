/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.solemn3.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.solemn3.dao.exceptions.NonexistentEntityException;
import root.solemn3.dao.exceptions.PreexistingEntityException;
import root.solemn3.entity.Infovacuna;

/**
 *
 * @author csanjuana
 */
public class InfovacunaJpaController implements Serializable {

    public InfovacunaJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("vacunas_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Infovacuna infovacuna) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(infovacuna);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findInfovacuna(infovacuna.getRut()) != null) {
                throw new PreexistingEntityException("Infovacuna " + infovacuna + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Infovacuna infovacuna) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            infovacuna = em.merge(infovacuna);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = infovacuna.getRut();
                if (findInfovacuna(id) == null) {
                    throw new NonexistentEntityException("The infovacuna with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Infovacuna infovacuna;
            try {
                infovacuna = em.getReference(Infovacuna.class, id);
                infovacuna.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The infovacuna with id " + id + " no longer exists.", enfe);
            }
            em.remove(infovacuna);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Infovacuna> findInfovacunaEntities() {
        return findInfovacunaEntities(true, -1, -1);
    }

    public List<Infovacuna> findInfovacunaEntities(int maxResults, int firstResult) {
        return findInfovacunaEntities(false, maxResults, firstResult);
    }

    private List<Infovacuna> findInfovacunaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Infovacuna.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Infovacuna findInfovacuna(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Infovacuna.class, id);
        } finally {
            em.close();
        }
    }

    public int getInfovacunaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Infovacuna> rt = cq.from(Infovacuna.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
