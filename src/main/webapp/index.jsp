<%-- 
    Document   : index
    Created on : 13-07-2021, 22:16:31
    Author     : csanjuana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Solemne N°3</title>
    </head>
    <body>
        <h1>Endpoints API vacunas </h1>
        <h1> Api de lista Vacunas - GET - https://solemn3.herokuapp.com/api/vacunas  </h1>
        <h1> Api de lista Vacunas - POST - https://solemn3.herokuapp.com/api/vacunas  </h1>
        <h1> Api de lista Vacunas - PUT - https://solemn3.herokuapp.com/api/vacunas  </h1>
        <h1> Api de lista Vacunas - DELETE - https://solemn3.herokuapp.com/api/vacunas/ideliminar (rut de la persona)  </h1>
        <h1> Api de lista Vacunas - GET por id - https://solemn3.herokuapp.com/api/vacunas/idConsultar (rut de la persona)  </h1>
    </body>
</html>
